package com.bing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BingstudyApplication {

    public static void main(String[] args) {
        SpringApplication.run(BingstudyApplication.class, args);
    }

}
